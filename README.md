# compress-bulk-image

This project created using simple python code, mainly to recursivelly compress every image in a single folder, and outputing by replacing the image. This project created to compress insanely huge sized image in my clients web-server.

This project contains one external plugins for python called Pillow, you can install the packages by running this command:

`pip install Pillow`

After installing the packages, download the .py project, and simply open terminal and cd to the folder where imgResize.py is located. Eg: Downloads folder

`cd ~/Downloads`

Simply run the program by telling where the folder you want to compress all of its contents.
For example, you want to compress all of your images in `~/Downloads/yourImages/compressme`

`echo "yourImages/compressme" | python imgResize.py`

All of your images on that folder will be replaced with compressed file, from 5mb file can be down to under 500kb. It is recommended to make a backup before you proceed the compressing file, because **this is permanent**!