import os
import sys
from PIL import Image

def compress_images(directory=False, quality=30):
    if directory:
        os.chdir(directory)

    files = os.listdir()
    images = [file for file in files if file.endswith(('jpg','jpeg','png'))]

    for image in images:
        img = Image.open(image)
        img.thumbnail((600, 600))
        img.save(image, optimize=True)
        print(image)

userInput = sys.stdin.read().replace('\n','')
compress_images(userInput,30)
